import ballerina/grpc;
import ballerina/protobuf.types.empty;
import ballerina/protobuf.types.wrappers;

public isolated client class ManagementClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR_PROBLEM1, getDescriptorMapProblem1());
    }

    isolated remote function create_course(Course|ContextCourse req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        Course message;
        if req is ContextCourse {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/create_course", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function create_courseContext(Course|ContextCourse req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        Course message;
        if req is ContextCourse {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/create_course", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function assign_course(Course|ContextCourse req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        Course message;
        if req is ContextCourse {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/assign_course", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function assign_courseContext(Course|ContextCourse req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        Course message;
        if req is ContextCourse {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/assign_course", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function submit_assignment(Assignment|ContextAssignment req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        Assignment message;
        if req is ContextAssignment {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/submit_assignment", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function submit_assignmentContext(Assignment|ContextAssignment req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        Assignment message;
        if req is ContextAssignment {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/submit_assignment", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function request_assignment(string|wrappers:ContextString req) returns Assignment|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if req is wrappers:ContextString {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/request_assignment", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <Assignment>result;
    }

    isolated remote function request_assignmentContext(string|wrappers:ContextString req) returns ContextAssignment|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if req is wrappers:ContextString {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/request_assignment", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <Assignment>result, headers: respHeaders};
    }

    isolated remote function create_user(User|ContextUser req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        User message;
        if req is ContextUser {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/create_user", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function create_userContext(User|ContextUser req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        User message;
        if req is ContextUser {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/create_user", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function register(Student|ContextStudent req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        Student message;
        if req is ContextStudent {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/register", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function registerContext(Student|ContextStudent req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        Student message;
        if req is ContextStudent {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/register", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function getAllStudents() returns string|grpc:Error {
        empty:Empty message = {};
        map<string|string[]> headers = {};
        var payload = check self.grpcClient->executeSimpleRPC("Management/getAllStudents", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function getAllStudentsContext() returns wrappers:ContextString|grpc:Error {
        empty:Empty message = {};
        map<string|string[]> headers = {};
        var payload = check self.grpcClient->executeSimpleRPC("Management/getAllStudents", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function login(pswd|ContextPswd req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        pswd message;
        if req is ContextPswd {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/login", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function loginContext(pswd|ContextPswd req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        pswd message;
        if req is ContextPswd {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/login", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function getUser(string|wrappers:ContextString req) returns User|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if req is wrappers:ContextString {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/getUser", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <User>result;
    }

    isolated remote function getUserContext(string|wrappers:ContextString req) returns ContextUser|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if req is wrappers:ContextString {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/getUser", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <User>result, headers: respHeaders};
    }

    isolated remote function getStudent(string|wrappers:ContextString req) returns Student|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if req is wrappers:ContextString {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/getStudent", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <Student>result;
    }

    isolated remote function getStudentContext(string|wrappers:ContextString req) returns ContextStudent|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if req is wrappers:ContextString {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/getStudent", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <Student>result, headers: respHeaders};
    }

    isolated remote function getAllUsers() returns string|grpc:Error {
        empty:Empty message = {};
        map<string|string[]> headers = {};
        var payload = check self.grpcClient->executeSimpleRPC("Management/getAllUsers", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function getAllUsersContext() returns wrappers:ContextString|grpc:Error {
        empty:Empty message = {};
        map<string|string[]> headers = {};
        var payload = check self.grpcClient->executeSimpleRPC("Management/getAllUsers", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function setUser(partUser|ContextPartUser req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        partUser message;
        if req is ContextPartUser {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/setUser", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function setUserContext(partUser|ContextPartUser req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        partUser message;
        if req is ContextPartUser {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("Management/setUser", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }
}

public client class ManagementStudentCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendStudent(Student response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextStudent(ContextStudent response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class ManagementUserCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendUser(User response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextUser(ContextUser response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class ManagementStringCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendString(string response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextString(wrappers:ContextString response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class ManagementAssignmentCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendAssignment(Assignment response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextAssignment(ContextAssignment response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextAssignment record {|
    Assignment content;
    map<string|string[]> headers;
|};

public type ContextUser record {|
    User content;
    map<string|string[]> headers;
|};

public type ContextPswd record {|
    pswd content;
    map<string|string[]> headers;
|};

public type ContextCourse record {|
    Course content;
    map<string|string[]> headers;
|};

public type ContextStudent record {|
    Student content;
    map<string|string[]> headers;
|};

public type ContextPartUser record {|
    partUser content;
    map<string|string[]> headers;
|};

public type Assignment record {|
    string student = "";
    string course = "";
    string assignment = "";
    string mark = "";
|};

public type User record {|
    string id = "";
    string name = "";
    string address = "";
    string profile_type = "";
    string password = "";
|};

public type pswd record {|
    string username = "";
    string password = "";
|};

public type Assessment record {|
    string assignment = "";
    string weight = "";
|};

public type Assessor record {|
    string name = "";
    string address = "";
    string course = "";
|};

public type Student record {|
    string std_num = "";
    string name = "";
    string[] courses = [];
|};

public type Course record {|
    string course_code = "";
    string name = "";
    Assessment[] assessments = [];
    string assessor = "";
|};

public type partUser record {|
    User user = {};
    string courseName = "";
|};

const string ROOT_DESCRIPTOR_PROBLEM1 = "0A0E70726F626C656D312E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F1A1B676F6F676C652F70726F746F6275662F656D7074792E70726F746F2283010A0455736572120E0A0269641801200128095202696412120A046E616D6518022001280952046E616D6512180A076164647265737318032001280952076164647265737312210A0C70726F66696C655F74797065180420012809520B70726F66696C6554797065121A0A0870617373776F7264180520012809520870617373776F726422500A084173736573736F7212120A046E616D6518012001280952046E616D6512180A076164647265737318022001280952076164647265737312160A06636F757273651803200128095206636F7572736522500A0753747564656E7412170A077374645F6E756D18012001280952067374644E756D12120A046E616D6518022001280952046E616D6512180A07636F75727365731803200328095207636F757273657322440A0A4173736573736D656E74121E0A0A61737369676E6D656E74180120012809520A61737369676E6D656E7412160A0677656967687418022001280952067765696768742288010A06436F75727365121F0A0B636F757273655F636F6465180120012809520A636F75727365436F646512120A046E616D6518022001280952046E616D65122D0A0B6173736573736D656E747318032003280B320B2E4173736573736D656E74520B6173736573736D656E7473121A0A086173736573736F7218042001280952086173736573736F7222720A0A41737369676E6D656E7412180A0773747564656E74180120012809520773747564656E7412160A06636F757273651802200128095206636F75727365121E0A0A61737369676E6D656E74180320012809520A61737369676E6D656E7412120A046D61726B18042001280952046D61726B223E0A0470737764121A0A08757365726E616D651801200128095208757365726E616D65121A0A0870617373776F7264180220012809520870617373776F726422450A08706172745573657212190A047573657218012001280B32052E55736572520475736572121E0A0A636F757273654E616D65180220012809520A636F757273654E616D6532BA050A0A4D616E6167656D656E7412360A0D6372656174655F636F7572736512072E436F757273651A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512360A0D61737369676E5F636F7572736512072E436F757273651A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565123E0A117375626D69745F61737369676E6D656E74120B2E41737369676E6D656E741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565123F0A12726571756573745F61737369676E6D656E74121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A0B2E41737369676E6D656E7412320A0B6372656174655F7573657212052E557365721A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512320A08726567697374657212082E53747564656E741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512460A0E676574416C6C53747564656E747312162E676F6F676C652E70726F746F6275662E456D7074791A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565122C0A056C6F67696E12052E707377641A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565122E0A0767657455736572121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A052E5573657212340A0A67657453747564656E74121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A082E53747564656E7412430A0B676574416C6C557365727312162E676F6F676C652E70726F746F6275662E456D7074791A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512320A077365745573657212092E70617274557365721A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565620670726F746F33";

public isolated function getDescriptorMapProblem1() returns map<string> {
    return {"google/protobuf/empty.proto": "0A1B676F6F676C652F70726F746F6275662F656D7074792E70726F746F120F676F6F676C652E70726F746F62756622070A05456D70747942540A13636F6D2E676F6F676C652E70726F746F627566420A456D70747950726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "problem1.proto": "0A0E70726F626C656D312E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F1A1B676F6F676C652F70726F746F6275662F656D7074792E70726F746F2283010A0455736572120E0A0269641801200128095202696412120A046E616D6518022001280952046E616D6512180A076164647265737318032001280952076164647265737312210A0C70726F66696C655F74797065180420012809520B70726F66696C6554797065121A0A0870617373776F7264180520012809520870617373776F726422500A084173736573736F7212120A046E616D6518012001280952046E616D6512180A076164647265737318022001280952076164647265737312160A06636F757273651803200128095206636F7572736522500A0753747564656E7412170A077374645F6E756D18012001280952067374644E756D12120A046E616D6518022001280952046E616D6512180A07636F75727365731803200328095207636F757273657322440A0A4173736573736D656E74121E0A0A61737369676E6D656E74180120012809520A61737369676E6D656E7412160A0677656967687418022001280952067765696768742288010A06436F75727365121F0A0B636F757273655F636F6465180120012809520A636F75727365436F646512120A046E616D6518022001280952046E616D65122D0A0B6173736573736D656E747318032003280B320B2E4173736573736D656E74520B6173736573736D656E7473121A0A086173736573736F7218042001280952086173736573736F7222720A0A41737369676E6D656E7412180A0773747564656E74180120012809520773747564656E7412160A06636F757273651802200128095206636F75727365121E0A0A61737369676E6D656E74180320012809520A61737369676E6D656E7412120A046D61726B18042001280952046D61726B223E0A0470737764121A0A08757365726E616D651801200128095208757365726E616D65121A0A0870617373776F7264180220012809520870617373776F726422450A08706172745573657212190A047573657218012001280B32052E55736572520475736572121E0A0A636F757273654E616D65180220012809520A636F757273654E616D6532BA050A0A4D616E6167656D656E7412360A0D6372656174655F636F7572736512072E436F757273651A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512360A0D61737369676E5F636F7572736512072E436F757273651A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565123E0A117375626D69745F61737369676E6D656E74120B2E41737369676E6D656E741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565123F0A12726571756573745F61737369676E6D656E74121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A0B2E41737369676E6D656E7412320A0B6372656174655F7573657212052E557365721A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512320A08726567697374657212082E53747564656E741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512460A0E676574416C6C53747564656E747312162E676F6F676C652E70726F746F6275662E456D7074791A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565122C0A056C6F67696E12052E707377641A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565122E0A0767657455736572121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A052E5573657212340A0A67657453747564656E74121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A082E53747564656E7412430A0B676574416C6C557365727312162E676F6F676C652E70726F746F6275662E456D7074791A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512320A077365745573657212092E70617274557365721A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565620670726F746F33"};
}

