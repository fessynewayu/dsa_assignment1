// This is the client implementation of the simple RPC scenario.
import ballerina/io;

// Creates a gRPC client to interact with the remote server.
ManagementClient ep = check new ("http://localhost:9090");

public function main() returns error? {
    io:println(mainMenu());

}

function mainMenu() returns string|error{

    string|error result="";

    string menu = ("----------------------\n"+
                  "|      SYSTEM MENU     |\n"+
                  "------------------------\n"+
                  "|                      |\n"+
                  "| 1) Sign Up:          |\n"+
                  "|                      |\n"+
                  "| 2) Login:            |\n"+
                  "| 0) exit:             |\n"+
                  "|                      |\n"+
                  "|Choose...:            |\n"+
                  "------------------------\n");

    string input= io:readln(menu);  

    if(input=="1") {
          result=signUp();
    }

    if(input=="2") {
          result  = login();
    }

    return result;

}








function login() returns string|error {

    string id= io:readln("Enter User Id: "); 
    string password= io:readln("Enter User Password: "); 

    User user = check ep->getUser(id);
    string res = "";
    string|error pro = "";
    //string student =viewProfile(user);
   

    if(user.password==password) 
    {
         res = user.profile_type;

         if(res == "learner") {
            pro = studentMenu(user);             
            }
        
        if(res == "assessor") {
            pro = assessorMenu(user);             
            }

        if(res == "assessor") {
            pro = adminMenu(user);             
            }
           
    }
    else
    {   pro = "Wrong Password"; }

    
    return pro;
}



function adminMenu(User user) returns string|error {
    
    string|error result="";




    string menu = ("----------------------\n"+
                  "| ADMINISTRATOR MENU   |\n"+
                  "------------------------\n"+
                  "|                      |\n"+
                  "| 1) View Profile:     |\n"+
                  "| 2) Create Course     |\n"+
                  "|                      |\n"+
                  "| 3) Exit menu:        |\n"+
                  "|                      |\n"+
                  "|Choose...:            |\n"+
                  "------------------------\n");

    
    string input= io:readln(menu);  

    if(input=="1") {
          result=viewProfile(user);
    }

    if(input=="2") {
          result  =  createCourses();
    }




   
    
    return result;
}




function assessorMenu(User user) returns string|error {
    
    string|error result="";

    string menu = ("----------------------\n"+
                  "|     ASSESSOR MENU    |\n"+
                  "------------------------\n"+
                  "|                      |\n"+
                  "| 1) View Profile:     |\n"+
                  "| 2) Request Assignment|\n"+
                  "| 3) Mark Assignment  |\n"+
                  "| 4) Exit menu:        |\n"+
                  "|                      |\n"+
                  "|Choose...:            |\n"+
                  "------------------------\n");

    
    string input= io:readln(menu);  

    if(input=="1") {
          result=viewProfile(user);
    }

    if(input=="2") {
          result  = requestAssignment();
    }

    if(input=="3") {
          result  = submitMarks();
    }
    
 
    return result;
}

function requestAssignment() returns string|error{
    
    string std = io:readln("Enter student Number...");

    Assignment asgmnt =  check ep->request_assignment(std);

    return asgmnt.assignment;    
}


function submitMarks() returns string|error{
    
      string std = io:readln("Enter student Number...");

      Assignment student =  check ep->request_assignment(std);

      string Mark = io:readln("Enter mark...");
      //int|error Mark = int:fromString(input);
     
       Assignment asgmnt = {student: student.student,
                           course: student.course,
                           assignment: student.assignment,
                           mark: Mark
                           };

       string result = check ep->submit_assignment(asgmnt);

      return result;
}



function viewProfile(User user) returns string {
        
 

          return ("Student Id_: "+user.id+"\n"+
                 "User Name_: "+user.name+"\n"+
                 "Home Address_: "+user.address+"\n");
}


function studentMenu (User user) returns string|error {
    
    string|error result="";

    string menu = ("----------------------\n"+
                  "|      STUDENT MENU    |\n"+
                  "------------------------\n"+
                  "|                      |\n"+
                  "| 1) View Profile:     |\n"+
                  "| 2) Submit Assignment:|\n"+
                  "| 3) Register Course:  |\n"+
                  "| 4) Exit menu:        |\n"+
                  "|                      |\n"+
                  "|Choose...:            |\n"+
                  "------------------------\n");

    
    string input= io:readln(menu);  

    if(input=="1") {
          result=viewProfile(user);
    }

    if(input=="2") {
          result  = submitAssignment(user);
    }

    if(input=="3") {
         result = toRegister(user);
    }

    if(input=="4") {
          result  = "Under cONSTRUCTION";
    }
    
    return result;
}


function toRegister(User user) returns string|error{
    
    string course = io:readln("Type Course...");

    string[] courses =["",course]; 

     Student theStudent = {std_num: user.id,
                           name: user.name,
                           courses: courses};


    string result = check ep->register(theStudent);

    return result;

}

function submitAssignment(User user) returns string|error{
    
    string Course = io:readln("Type Course...");
    string assg = io:readln("Assignment Title...");

//if(course=="") 

     Assignment asgmnt = {student: user.name,
                           course: Course,
                           assignment: assg,
                           mark: ""
                           };


    string result = check ep->submit_assignment(asgmnt);

    return result;

}



function signUp() returns string|error {

    string UserId = io:readln("Enter Id_:");
    string userName = io:readln("Enter Name_:");
    string userAddress = io:readln("Enter Home Address_:");
    string profileType = io:readln("Enter Profile type_:");
    string userPassword = io:readln("Enter Passwword_:");


    User user = {
                 id: UserId,
                 name: userName, 
                 address: userAddress,
                 profile_type: profileType,
                 password: userPassword
                 };

   
    // Executes a simple remote call.
    string result = check ep->create_user(user);
    // Prints the received result.
    io:print(result);

    return mainMenu();

}


function createCourses() returns string|error {

   

    string courseCode = io:readln("Couse code_:");
    string courseName = io:readln("Enter Course Name_:");
    string Assignment = io:readln("Enter Assessement_:");
     string Weight = io:readln("Enter Assessement Weight_:");
    string assessor = io:readln("Enter Profile type_:");
     

    Assessment a = {assignment: Assignment,weight:Weight};
    
    Assessment[] assements = [a];



    Course course = {
                 course_code: courseCode,
                 name: courseName, 
                 assessments: assements,
                 assessor: assessor
                 };

   
    // Executes a simple remote call.
    string result = check ep->create_course(course);
    // Prints the received result.
    io:print(result);

    return mainMenu();

}


