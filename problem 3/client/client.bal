import ballerina/http;

public isolated client class Client {
    final http:Client clientEp;
    # Gets invoked to initialize the `connector`.
    #
    # + clientConfig - The configurations to be used when initializing the `connector` 
    # + serviceUrl - URL of the target service 
    # + return - An error if connector initialization failed 
    public isolated function init(http:ClientConfiguration clientConfig =  {}, string serviceUrl = "http://localhost:8080/students/api/v1") returns error? {
        http:Client httpEp = check new (serviceUrl, clientConfig);
        self.clientEp = httpEp;
        return;
    }
    # Get all students added to the application
    #
    # + return - A list of students 
    remote isolated function getAll() returns Student[]|error {
        string resourcePath = string `/students`;
        Student[] response = check self.clientEp->get(resourcePath);
        return response;
    }
    # Insert a new student
    #
    # + return - Student successfully created 
    remote isolated function insert(Student payload) returns InlineResponse201|error {
        string resourcePath = string `/students`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody, "application/json");
        InlineResponse201 response = check self.clientEp->post(resourcePath, request);
        return response;
    }
    # Get a single upser
    #
    # + studentnumber - studentnumber of the student 
    # + return - student response 
    remote isolated function getStudent(int studentnumber) returns Student|error {
        string resourcePath = string `/users/${getEncodedUri(studentnumber)}`;
        Student response = check self.clientEp->get(resourcePath);
        return response;
    }
    # Update an existing student
    #
    # + studentnumber - studentnumber of the student 
    # + return - Student was successfully updated 
    remote isolated function updateStudent(int studentnumber, Student payload) returns Student|error {
        string resourcePath = string `/users/${getEncodedUri(studentnumber)}`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody, "application/json");
        Student response = check self.clientEp->put(resourcePath, request);
        return response;
    }
    # Delete an existing student
    #
    # + studentnumber - studentnumber of the student 
    # + return - Student was successfully deleted 
    remote isolated function deleteUser(int studentnumber) returns http:Response|error {
        string resourcePath = string `/users/${getEncodedUri(studentnumber)}`;
        http:Response response = check self.clientEp->delete(resourcePath);
        return response;
    }
}
