import ballerina/http;

listener http:Listener ep0 = new (8080, config = {host: "localhost"});

service /students/api/v1 on ep0 {
    resource function get students() returns Student[]|http:Response {
    }
    resource function post students(@http:Payload Student payload) returns CreatedInlineResponse201|http:Response {
    }
    resource function get users/[int  studentnumber]() returns Student|http:Response {
    }
    resource function put users/[int  studentnumber](@http:Payload Student payload) returns Student|http:Response {
    }
    resource function delete users/[int  studentnumber]() returns http:NoContent|http:Response {
    }
}
