import ballerina/http;

public type CreatedInlineResponse201 record {|
    *http:Created;
    InlineResponse201 body;
|};

public type StudentArr Student[];

public type InlineResponse201 record {
    # the studentnumber of the student newly created
    int studentnumber?;
};

public type Error record {
    string errorType?;
    string message?;
};

public type Student record {
    # the studentnumber of the student
    int studentnumber?;
    # the studentname of the student
    string studentname?;
    # the email address of the student
    string email?;
};
