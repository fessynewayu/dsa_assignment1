import ballerina/io;
import ballerina/graphql;
import ballerina/http;

// data type
// record type
public type CovidEntry record {|
    readonly string regionCode;
    string date;
    string region;
    decimal deaths?;
    decimal confirmed_cases?;
    decimal recoveries?;
    decimal tested?;
|};

// data table
table<CovidEntry> key(regionCode) covidEntriesTable = table [
    {regionCode: "061", date: "12/09/2021", region: "Khomas", deaths: 39, confirmed_cases: 465,  recoveries: 67, tested: 1200} 
];

// Object type
public distinct service class CovidData {
    private final readonly & CovidEntry entryRecord;

    function init(CovidEntry entryRecord) {
        self.entryRecord = entryRecord.cloneReadOnly();
    }

    resource function get regionCode() returns string {
        return self.entryRecord.regionCode;
    }

    resource function get date() returns string {
        return self.entryRecord.date;
    }

    resource function get region() returns string {
        return self.entryRecord.region;
    }

    resource function get deaths() returns decimal? {
        if self.entryRecord.deaths is decimal {
            return self.entryRecord.deaths / 1000;
        }
        return;
    }

    resource function get confirmed_cases() returns decimal? {
        if self.entryRecord.confirmed_cases is decimal {
            return self.entryRecord.confirmed_cases/ 1000;
        }
        return;
    }

    resource function get recoveries() returns decimal? {
        if self.entryRecord.recoveries is decimal {
            return self.entryRecord.recoveries/ 1000;
        }
        return;
    }

    resource function get tested() returns decimal? {
        if self.entryRecord.tested is decimal {
            return self.entryRecord.tested/ 1000;
        }
        return;
    }
}
// initializing the Listener using an HTTP Listener
listener http:Listener httpListener = check new(9991);
// Service
service /covid19_statistics on new graphql:Listener(httpListener) {

    // Query type - all field
    resource function get all() returns CovidData[] {
        CovidEntry[] covidEntries = covidEntriesTable.toArray().cloneReadOnly();
        return covidEntries.map(entry => new CovidData(entry));
    }
    // Query type - filter field
    resource function get filter(string regionCode) returns CovidData? {
        CovidEntry? covidEntry = covidEntriesTable[regionCode];
        if covidEntry is CovidEntry {
            return new (covidEntry);
        }
        return;
    }

    // Mutation type - update field
    remote function add(CovidEntry entry) returns CovidData {
        covidEntriesTable.add(entry);
        return new CovidData(entry);
    }
}

public function main() {
    io:println("COVID19 STATISTICS");
}
